package test;
import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase
{
	private Stack<String> stack1;
	
	public void setupEscenario1()
	{
		try
		{
			stack1 = new Stack();
		}
		catch(Exception E)
		{
			fail("No deberia lanzar Excepci�n");
		}
	}
	
	public void testPush()
	{
		setupEscenario1();
		stack1.push("Elemento A");
		stack1.push("Elemento B");
		stack1.push("Elemento C");
		assertEquals("Elemento C", stack1.darElemento(0));
		assertEquals("Elemento B", stack1.darElemento(1));
		assertEquals("Elemento A", stack1.darElemento(2));
	}
	
	public void testPop()
	{
		setupEscenario1();
		stack1.push("Elemento A");
		stack1.push("Elemento B");
		stack1.push("Elemento C");
		stack1.pop();
		assertEquals("Elemento B", stack1.darElemento(0));
		stack1.pop();
		assertEquals("Elemento A", stack1.darElemento(0));
	}
}
