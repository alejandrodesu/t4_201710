package model.logic;

import java.util.ArrayList;
import java.util.Comparator;

import model.data_structures.Queue;
import model.data_structures.Stack;

public class Manejador
{
	private String cadena;
	Stack stackChar = new Stack(null);
	public static final char REDONDO_ABIERTO = '(';
	public static final char REDONDO_CERRADO = ')';
	public static final char CUADRADO_ABIERTO = '[';
	public static final char CUADRADO_CERRADO = ']';
	public static final char SUMA = '+';
	public static final char RESTA = '-';
	public static final char MULTIPLICACION = '*';
	public static final char DIVISION = '/';
	
	public boolean expresionBienFormada(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		boolean expr = false;
		
		
		
		if(mismoNumero(expresion)==true);
		{
			if(parentesisSeguidos(expresion)==true)
			{
				if(dosSimbolosSeguidos(expresion)==true)
				{
					if(soloSimbolos(expresion))
					{
						if(malCerrado(expresion))
						{
							expr=true;
						}
					}
				}
			}
		}
		
		if(expr==true)
		{
			for(int i=0;i<expresionArray.length;i++)
			{
				stackChar.push(expresionArray[i]);
			}
		}
		
		return expr;
	}
	
	public boolean mismoNumero(String expresion)
	{
		boolean retorno = false;
			//Comprobar que haya la misma cantidad de parentesis de apertura y de cierre
				int cuentaAbiertoredondo = 0;
				int cuentaCerradoRedondo = 0;
				int cuentaAbiertocuadrado = 0;
				int cuentaCerradoCuadrado = 0;
				int cuentaFinal = 0;
				char[] expresionArray = expresion.toCharArray();
				for(int i=0; i<expresionArray.length;i++)
				{
					char actual = expresionArray[i];
					if(actual == REDONDO_ABIERTO)
					{
						cuentaAbiertoredondo++;
					}
					if(actual == REDONDO_CERRADO)
					{
						cuentaCerradoRedondo++;
					}
					if(actual == CUADRADO_ABIERTO)
					{
						cuentaAbiertocuadrado++;
					}
					if(actual == CUADRADO_CERRADO)
					{
						cuentaCerradoCuadrado++;
					}
				}
				if(cuentaAbiertoredondo==cuentaCerradoRedondo)
				{
					if(cuentaAbiertocuadrado==cuentaCerradoCuadrado)
					{
						retorno = true;
					}
				}
				return retorno;
			
	}
	
	//Forma [()]
	public boolean parentesisSeguidos(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		int count=0;
		//Comprobar que no haya () o []
		for(int i=0; i<expresionArray.length-1;i++)
		{
			char actual = expresionArray[i];
			
			char siguiente = expresionArray[i+1];
			if(actual==REDONDO_ABIERTO&&siguiente==REDONDO_CERRADO)
			{
				count++;
			}
			if(actual==CUADRADO_ABIERTO&&actual==CUADRADO_CERRADO)
			{
				count++;
			}
		}
		if(count==0)
		{
			return true;
		}
		else
			return false;
	}
	
	//Forma (5++3)
	public boolean dosSimbolosSeguidos(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		int count=0;
		for(int i = 0; i<expresionArray.length-1;i++)
		{
			char actual = expresionArray[i];
			char siguiente = expresionArray[i+1];
			if(esSimbolo(actual)&&esSimbolo(siguiente))
			{
				count++;
			}
		}
		if(count==0)
		{
			return true; 
		}
		else
			return false;
	}
	
	//Forma [(+-++)]
	public boolean soloSimbolos(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		int count = 0;
		boolean soloSimbolos=false;
		for(int i = 0; i<expresionArray.length;i++)
		{
			char actual = expresionArray[i];

			if(esNumero(actual))
			{
				count++;
			}
		}
		if(count>0)
		{
			soloSimbolos = true;
		}
		return soloSimbolos;
	}
	
	//Forma [(5+3])
	public boolean malCerrado(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		ArrayList posicionesAbiertosCuadrados = new ArrayList();
		ArrayList posicionesAbiertosRedondos = new ArrayList();
		ArrayList posicionesCerradosCuadrados = new ArrayList();
		ArrayList posicionesCerradosRedondos = new ArrayList();
		int count = 0;
		for(int i=0;i<expresionArray.length;i++)
		{
			char actual = expresionArray[i];
			if(actual==CUADRADO_ABIERTO)
			{
				posicionesAbiertosCuadrados.add(i);
			}
			if(actual==CUADRADO_CERRADO)
			{
				posicionesCerradosCuadrados.add(i);
			}
			if(actual==REDONDO_ABIERTO)
			{
				posicionesAbiertosRedondos.add(i);
			}
			if(actual==REDONDO_CERRADO)
			{
				posicionesCerradosRedondos.add(i);
			}
		}
		if((Integer) posicionesAbiertosCuadrados.get(0)>(Integer) posicionesAbiertosRedondos.get(0))
		{
			count++;
		}
		if((Integer) posicionesCerradosCuadrados.get(posicionesCerradosCuadrados.size()-1)<(Integer) posicionesCerradosRedondos.get(posicionesCerradosRedondos.size()-1))
		{
			count++;
		}
		
		if(count==0)
			return true;
		else
			return false;
		
	}
	
	public boolean esSimbolo(char simbolo)
	{
		if(simbolo==SUMA)
			return true;
		if(simbolo==RESTA)
			return true;
		if(simbolo==MULTIPLICACION)
			return true;
		if(simbolo==DIVISION)
			return true;
		else
			return false;
	}
	
	public boolean esParentesis(char simbolo)
	{
		if(simbolo==REDONDO_ABIERTO)
			return true;
		if(simbolo==REDONDO_CERRADO)
			return true;
		if(simbolo==CUADRADO_ABIERTO)
			return true;
		if(simbolo==CUADRADO_CERRADO)
			return true;
		else 
			return false;
	}
	
	public boolean esNumero(char simbolo)
	{
		if(simbolo=='0')
			return true;
		if(simbolo=='1')
			return true;
		if(simbolo=='2')
			return true;
		if(simbolo=='3')
			return true;
		if(simbolo=='4')
			return true;
		if(simbolo=='5')
			return true;
		if(simbolo=='6')
			return true;
		if(simbolo=='7')
			return true;
		if(simbolo=='8')
			return true;
		if(simbolo=='9')
			return true;
		else
			return false;
	}
	
	//Esta un poco chamb�n pero funciona
	public String ordenarPila()
	{
		
		Stack<Character> pila = stackChar;
		System.out.println(stackChar.darElemento(0));
		int cuentaCorcheteA = 0;
		int cuentaCorcheteC = 0;
		int cuentaParentesisA = 0;
		int cuentaParentesisC = 0;
		int cuentaSuma=0;
		int cuentaR=0;
		int cuentaM=0;
		int cuentaD=0;
		int cuenta0 = 0;
		int cuenta1 = 0;
		int cuenta2 = 0;
		int cuenta3 = 0;
		int cuenta4 = 0;
		int cuenta5 = 0;
		int cuenta6 = 0;
		int cuenta7 = 0;
		int cuenta8 = 0;
		int cuenta9 = 0;
		Queue aux = new Queue();
		String retorno = "";
		for(int i=0;i<pila.size();i++)
		{
			if(pila.darElemento(i)==CUADRADO_ABIERTO)
			{
				cuentaCorcheteA++;
			}
			else if(pila.darElemento(i)==CUADRADO_CERRADO)
			{
				cuentaCorcheteC++;
			}
			else if(pila.darElemento(i)==REDONDO_ABIERTO)
			{
				cuentaParentesisA++;
			}
			else if(pila.darElemento(i)==REDONDO_CERRADO)
			{
				cuentaParentesisC++;
			}
			else if(pila.darElemento(i)==SUMA)
			{
				cuentaSuma++;
			}
			else if(pila.darElemento(i)==RESTA)
			{
				cuentaR++;
			}
			else if(pila.darElemento(i)==MULTIPLICACION)
			{
				cuentaM++;
			}
			else if(pila.darElemento(i)==DIVISION)
			{
				cuentaD++;
			}
			else if(pila.darElemento(i)=='0')
			{
				cuenta0++;
			}
			else if(pila.darElemento(i)=='1')
			{
				cuenta1++;
			}
			else if(pila.darElemento(i)=='2')
			{
				cuenta2++;
			}
			else if(pila.darElemento(i)=='3')
			{
				cuenta3++;
			}
			else if(pila.darElemento(i)=='4')
			{
				cuenta4++;
			}
			else if(pila.darElemento(i)=='5')
			{
				cuenta5++;
			}
			else if(pila.darElemento(i)=='6')
			{
				cuenta6++;
			}
			else if(pila.darElemento(i)=='7')
			{
				cuenta7++;
			}
			else if(pila.darElemento(i)=='8')
			{
				cuenta8++;
			}
			else if(pila.darElemento(i)=='9')
			{
				cuenta9++;
			}
		}
		if(cuentaCorcheteC>0)
		{
			for(int i=0;i<cuentaCorcheteC;i++)
			{
				aux.enqueue(']');
			}
		}
		if(cuentaParentesisC>0)
		{
			for(int i=0;i<cuentaParentesisC;i++)
			{
				aux.enqueue(')');
			}
		}
		if(cuentaCorcheteA>0)
		{
			for(int i=0;i<cuentaCorcheteA;i++)
			{
				aux.enqueue('[');
			}
		}
		if(cuentaParentesisA>0)
		{
			for(int i=0;i<cuentaParentesisA;i++)
			{
				aux.enqueue('(');
			}
		}
		if(cuentaSuma>0)
		{
			for(int i=0;i<cuentaSuma;i++)
			{
				aux.enqueue('+');
			}
		}
		if(cuentaR>0)
		{
			for(int i=0;i<cuentaR;i++)
			{
				aux.enqueue('-');
			}
		}
		if(cuentaM>0)
		{
			for(int i=0;i<cuentaM;i++)
			{
				aux.enqueue('*');
			}
		}
		if(cuentaD>0)
		{
			for(int i=0;i<cuentaD;i++)
			{
				aux.enqueue('/');
			}
		}
		if(cuenta0>0)
		{
			for(int i=0;i<cuenta0;i++)
			{
				aux.enqueue('0');
			}
		}
		if(cuenta1>0)
		{
			for(int i=0;i<cuenta1;i++)
			{
				aux.enqueue('1');
			}
		}
		if(cuenta2>0)
		{
			for(int i=0;i<cuenta2;i++)
			{
				aux.enqueue('2');
			}
		}
		if(cuenta3>0)
		{
			for(int i=0;i<cuenta3;i++)
			{
				aux.enqueue('3');
			}
		}
		if(cuenta4>0)
		{
			for(int i=0;i<cuenta4;i++)
			{
				aux.enqueue('4');
			}
		}
		if(cuenta5>0)
		{
			for(int i=0;i<cuenta5;i++)
			{
				aux.enqueue('5');
			}
		}
		if(cuenta6>0)
		{
			for(int i=0;i<cuenta6;i++)
			{
				aux.enqueue('6');
			}
		}
		if(cuenta7>0)
		{
			for(int i=0;i<cuenta7;i++)
			{
				aux.enqueue('7');
			}
		}
		if(cuenta8>0)
		{
			for(int i=0;i<cuenta8;i++)
			{
				aux.enqueue('8');
			}
		}
		if(cuenta9>0)
		{
			for(int i=0;i<cuenta9;i++)
			{
				aux.enqueue('9');
			}
		}
		for(int i=0;i<aux.size();i++)
		{
			retorno = retorno + aux.darElemento(i)+"\n";
		}
		return retorno;
	}
}